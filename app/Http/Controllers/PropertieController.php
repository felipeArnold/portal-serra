<?php

namespace App\Http\Controllers;

use App\Models\{
    Hit,
    Propertie
};
use Illuminate\Contracts\View\View;
use Illuminate\Http\Request;
use Exception;
use Illuminate\Support\Facades\DB;

class PropertieController extends Controller
{

    public function index(Request $request): View
    {
        $search = isset($request->search) ? (string) $request->search : "";
        $perPage = 12;
        $page = isset($request['page']) ? (int) $request['page'] : 1;

        $cityFilter = str_replace('-', ' ', $request->cityFilter);
        $district = !empty($request['bairro']) ? (string) str_replace('-', ' ', $request['bairro']) : "";
        $type = !empty($request['tipo']) ? (string) str_replace('-', ' ', $request['tipo']) : "";

        $properties = Propertie::where(
            [
                ['city', $cityFilter]
            ]
        )->when(!empty($request['bairro']), function ($query) use ($district) {
            $query->where('district', $district);
        })->when(!empty($request['tipo']), function ($query) use ($type) {
            $query->where('type', $type);
        })->offset(($page - 1) * $perPage)
            ->paginate($perPage);

        $properties->cityFilter = $cityFilter;
        $properties->district = $district;
        $properties->type = $type;

        $properties->requestFilter = '&bairro=' .$district . '&tipo='.$type;

        return view('Propertie.properties', compact('properties'));
    }

    public function immobile(Request $request)
    {
        try {
            DB::beginTransaction();
            $hits = Hit::where('properties_key', $request->codePropertie)->first();

            $hitsViews = $hits ? $hits->views + 1 : 1;

            if ($hitsViews == 1) {
                $hits = new Hit();
                $hits->properties_key = $request->codePropertie;
            }

            $hits->views = $hitsViews;
            $hits->save();
            DB::commit();
        } catch (\Exception $e) {
            DB::rollBack();
            return response()->json(['error' => 'Não foi atuaizar a quantidade de acessos.'], 200);
        }

        $propertie = Propertie::where('key', $request->codePropertie)->first();

        return view('Propertie.propertie', compact('propertie'));
    }
}
