<?php

namespace App\Http\Middleware;

use App\Models\Propertie;
use Closure;
use Illuminate\Http\Request;

class VerifyPropertie
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure(\Illuminate\Http\Request): (\Illuminate\Http\Response|\Illuminate\Http\RedirectResponse)  $next
     * @return \Illuminate\Http\Response|\Illuminate\Http\RedirectResponse
     */
    public function handle(Request $request, Closure $next)
    {
        $data = Propertie::where('key', $request->route('codePropertie'));

        $propertie = $data->first();
        if (is_null($propertie)) {
            return redirect(route('imoveis-alugar/cidade', $request->route('cityFilter')));
        }

        return $next($request);
    }
}
