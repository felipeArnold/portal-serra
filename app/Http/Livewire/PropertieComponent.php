<?php

namespace App\Http\Livewire;

use App\Models\Propertie;
use Illuminate\Contracts\View\View;
use Livewire\Component;

class PropertieComponent extends Component
{
    public string $title;
    public string $description;
    public string $city;
    public string $district;
    public string $type;

    public function mount(
        string $city,
        string $district,
        string $type
    ): void {

        $this->city = $city;
        $this->district = $district;
        $this->type = $type;

        $this->properties = Propertie::where(
            [
                ['city', $this->city],
                ['district', $this->district],
                ['type', $this->type],
                ['state', 'RS'],
                ['goal', 'LOC']
            ]
        )->orderBy('key', 'desc')
            ->limit(8)
            ->get();
    }

    public function render(): View
    {
        return view('livewire.propertie', [$this]);
    }
}
