<?php

namespace App\Http\Livewire;

use App\Models\Propertie;
use Livewire\Component;

class FilterComponent extends Component
{

    public string $city;
    public string $district;
    public string $type;
    public string $order;
    public string $restroom;
    public string $bedroom;
    public string $garage;

    public function mount(
        string $city,
        string $district,
        string $type,
        string $order,
        string $restroom,
        string $bedroom,
        string $garage
    ): void {

        $this->city = $city;
        $this->district = $district;
        $this->type = $type;
        $this->order = $order;
        $this->restroom = $restroom;
        $this->bedroom = $bedroom;
        $this->garage = $garage;
    }

    public function render()
    {
        $this->types = Propertie::select('type')
            ->where(
                [
                    ['city', $this->city]
                ]
            )
            ->distinct()
            ->get();

        $this->districts = Propertie::select('district')
            ->where(
                [
                    ['city', $this->city]
                ]
            )
            ->distinct()
            ->get();

        return view('livewire.filter', [$this]);
    }
}
