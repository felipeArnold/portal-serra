<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\{
    HasMany,
    HasOne
};

class Propertie extends Model
{
    use HasFactory;

    protected static function boot()
    {
        parent::boot();

        static::addGlobalScope('propertie', function ($query) {
            $query->where(
                [
                    ['state', 'RS'],
                    ['goal', 'LOC']
                ]
            );
        });
    }

    public function price(): HasOne
    {
        return $this->hasOne(Price::class, 'properties_key', 'key');
    }

    public function advert(): HasOne
    {
        return $this->hasOne(Advert::class, 'properties_key', 'key');
    }

    public function improvement(): HasOne
    {
        return $this->hasOne(Improvement::class, 'properties_key', 'key');
    }

    public function area(): hasOne
    {
        return $this->hasOne(Area::class, 'properties_key', 'key');
    }

    public function image(): HasMany
    {
        return $this->hasMany(Image::class, 'properties_key', 'key');
    }

    public function hit(): hasOne
    {
        return $this->hasOne(Hit::class, 'properties_key', 'key');
    }
}
