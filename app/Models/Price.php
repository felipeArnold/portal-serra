<?php

namespace App\Models;

use App\Http\Controllers\PropertieController;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class Price extends Model
{
    use HasFactory;

    public function price(): BelongsTo
    {
        return $this->belongsTo(PropertieController::class);
    }
}
