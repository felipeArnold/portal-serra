<?php

use App\Http\Controllers\PropertieController;
use App\Http\Middleware\VerifyPropertie;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('home');
})->name('home');

Route::get('/quem-somos', function () {
    return view('company');
})->name('company');

Route::get('/fale-conosco', function () {
    return view('contact');
})->name('contact');

Route::get('/servicos', function () {
    return view('sevice');
})->name('sevice');

Route::prefix('/imoveis-alugar/cidade')->controller(PropertieController::class)->group(function () {

    Route::get('/', 'index')->name('rentCity');

    Route::prefix('/{cityFilter}')->group(function ($cityFilter) {

        Route::get('/', 'index')->name('propertie');

        Route::prefix('/{codePropertie?}')->middleware(VerifyPropertie::class)->group(function () {
            Route::get('/{namePropertie?}', 'immobile')->name('immobile');
        });
    });
});
