<div>
    <section class="feature-style-two sec-pad">
        <div class="auto-container">
            <div class="sec-title">
                <h5>{{ $title }}</h5>
                <h2>{{ $description }}</h2>
            </div>
            <div class="three-item-carousel owl-carousel owl-theme owl-dots-none nav-style-one">

                @foreach ($properties as $propertie)
                    @php
                        $urlPropertie = $propertie->city . '/' . $propertie->improvement->properties_key . '/' . str_replace(' ', '-', strtolower($propertie->advert->title));
                    @endphp

                    <div class="feature-block-one">
                        <div class="inner-box content-side">
                            <div class="image-box property-details-content">

                                <div class="carousel-inner">

                                    <div class="single-item-carousel owl-carousel owl-theme owl-dots-none">
                                        @if ($propertie->image->count() > 0)
                                            @foreach ($propertie->image as $image)
                                                @if ($image->order === 1 || $image->order === 2 || $image->order === 3)
                                                    <figure class="image-box">
                                                        <img src="https://media.samisistemas.com.br/{{ strtolower($propertie->initials) }}/data/imagens/aluguel/{{ $image->file }}?{{ date('YmdHsi') }}"
                                                            alt="{{ $propertie->advert->title }}"
                                                            style="max-height: 250px; min-height: 250px; max-width: 100%;">
                                                    </figure>
                                                @endif
                                            @endforeach
                                        @else
                                            <img src="https://www.barcellos.com.br/images/nao-disponivel.jpg"
                                                alt="{{ $propertie->advert->title }}"
                                                style="max-height: 250px; min-height: 250px; max-width: 100%;">
                                        @endif
                                    </div>

                                </div>
                                @if ($propertie->advert->offer == 'S')
                                    <div class="batch"><i class="icon-11"></i></div>
                                @endif
                                @if ($propertie->advert->emphasis == 'S')
                                    <span class="category">DESTAQUE</span>
                                @endif
                                @if ($propertie->advert->exclusive == 'S')
                                    <span class="category">EXCLUSIVO</span>
                                @endif
                            </div>
                            <div class="lower-content">
                                <div class="author-info clearfix">
                                    <div class="author pull-left">
                                        <h6>{{ $propertie->type }}</h6>
                                    </div>
                                    <div class="buy-btn pull-right"><a href="{{ $urlPropertie }}">
                                            {{ $propertie->improvement->properties_key }}
                                        </a>
                                    </div>
                                </div>
                                <div class="title-text">
                                    <h4 class="designation">
                                        <a href="{{ $urlPropertie }}" class="d-flex flex-column">
                                            <span>{{ $propertie->address . ', ' . $propertie->district . ', ' }}</span>
                                            <span>{{ $propertie->city . '/' . $propertie->state }}</span>
                                        </a>
                                    </h4>
                                </div>

                                <h3 class="title-description">
                                    {{ $propertie->advert->title }}
                                </h3>

                                <ul class="more-details clearfix">
                                    <li>
                                        <div class="d-flex flex-column">
                                            <span>
                                                <i class="icon-14"></i>
                                                {{ $propertie->improvement->dormitory }}
                                            </span>
                                            <span>
                                                {{ $propertie->improvement->dormitory == 1 ? ' dormitório' : ' dormitórios' }}
                                            </span>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="d-flex flex-column">
                                            <span>
                                                <i class="icon-15"></i>
                                                {{ $propertie->improvement->restroom }}
                                            </span>
                                            <span>
                                                {{ $propertie->improvement->restroom == 1 ? ' banheiro' : ' banheiro' }}
                                            </span>
                                        </div>
                                    </li>
                                    @if ($propertie->area->private)
                                        <li>
                                            <div class="d-flex flex-column">
                                                <span>
                                                    <i class="icon-16"></i>
                                                    {{ $propertie->area->private }} m²
                                                </span>
                                                <span>
                                                    Área privada
                                                </span>
                                            </div>
                                        </li>
                                    @endif
                                </ul>

                                <div class="price-box clearfix">
                                    <div class="price-info pull-left d-flex flex-column">
                                        @if (!is_null($propertie->price))
                                            <h6>Aluguel: R$
                                                {{ $propertie->price->rent ? number_format($propertie->price->rent, '2', ',', '.') : 'Consultar' }}
                                            </h6>
                                            <h6>Total: R$
                                                {{ number_format($propertie->price->rent + $propertie->price->condominium + $propertie->price->iptu, '2', ',', '.') }}
                                            </h6>
                                        @else
                                            <h6> Consultar valores </h6>
                                        @endif
                                    </div>
                                    <div class="btn-box pull-right">
                                        <a href="{{ $urlPropertie }}" class="theme-btn btn-two">
                                            Detalhes
                                        </a>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                @endforeach

            </div>
        </div>
    </section>
</div>
