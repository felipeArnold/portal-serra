<div>
    <section class="search-field-section" style="margin-top: 80px">
        <div class="auto-container">
            <div class="inner-container">
                <div class="search-field">
                    <div class="tabs-box">
                        <div class="tabs-content info-group">
                            <div class="tab active-tab" id="tab-1">
                                <div class="inner-box">
                                    <form action="{{ route('rentCity') }}/taquara" method="get">
                                        <div class="top-search">

                                            <div class="search-form">
                                                <div class="row clearfix">
                                                    <div class="col-lg-4 col-md-6 col-sm-12 column">
                                                        <div class="form-group">
                                                            <label>Bairro</label>
                                                            <div class="select-box">
                                                                <i class="far fa-location"></i>
                                                                <select class="wide" name="bairro">
                                                                    <option value="">Todos</option>
                                                                    <option value=""
                                                                        data-display="{{ $this->district ? $this->district : 'Selecione o bairro' }}">
                                                                    </option>
                                                                    @foreach ($districts as $district)
                                                                        <option value="{{ $district->district }}"
                                                                            {{ $this->district === $district->district ? 'selected' : '' }}>
                                                                            {{ $district->district }}</option>
                                                                    @endforeach
                                                                </select>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="col-lg-4 col-md-6 col-sm-12 column">
                                                        <div class="form-group">
                                                            <label>Tipos</label>
                                                            <div class="select-box">
                                                                <i class="fas fa-home"></i>
                                                                <select class="wide" name="tipo">
                                                                    <option value="">Todos</option>
                                                                    <option value=""
                                                                        data-display="{{ $this->type ? $this->type : 'Selecione o tipo' }}">
                                                                    </option>
                                                                    @foreach ($types as $key => $type)
                                                                        <option value="{{ $type->type }}"
                                                                            {{ $this->type === $type->type ? 'selected' : '' }}>
                                                                            {{ $type->type }}</option>
                                                                    @endforeach
                                                                </select>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-4 col-md-6 col-sm-12 column">
                                                        <div class="form-group">
                                                            <label>Ordenar</label>
                                                            <div class="select-box">
                                                                <select class="wide" name="ordenar">
                                                                    <option value="" data-display="Mais recentes">
                                                                        Mais recentes
                                                                    </option>
                                                                    <option value="Nova Hartz">Maior valor</option>
                                                                    <option value="Riozinho">Menor valor</option>
                                                                    <option value="Rolante">Maior área</option>
                                                                    <option value="Taquara">Menor área</option>
                                                                    <option value="Três Coroas">Mais visualizados
                                                                    </option>
                                                                </select>
                                                            </div>
                                                        </div>
                                                    </div>

                                                </div>
                                                <div class="search-btn">
                                                    <button type="submit">
                                                        <i class="fas fa-search"></i>Pesquisar
                                                    </button>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="switch_btn_one ">
                                            <button
                                                class="nav-btn nav-toggler navSidebar-button clearfix search__toggler">Filtrar
                                                <i class="fas fa-angle-down"></i>
                                            </button>
                                            <div class="advanced-search">
                                                <div class="close-btn">
                                                    <a href="#" class="close-side-widget">
                                                        <i class="far fa-times"></i>
                                                    </a>
                                                </div>
                                                <div class="row clearfix">
                                                    <div class="col-lg-4 col-md-6 col-sm-12 column">
                                                        <label>Quartos</label>
                                                        <div class="select-box">
                                                            <select class="wide" name="quarto">
                                                                <option value="" data-display="Quartos">Quartos
                                                                </option>
                                                                <option value="1">1 +</option>
                                                                <option value="2">2 +</option>
                                                                <option value="3">3 +</option>
                                                                <option value="4">4 +</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-4 col-md-6 col-sm-12 column">
                                                        <label>Banheiros</label>
                                                        <div class="select-box">
                                                            <select class="wide" name="banheiro">
                                                                <option value="" data-display="Banheiros">
                                                                    Banheiros </option>
                                                                <option value="1">1 +</option>
                                                                <option value="2">2 +</option>
                                                                <option value="3">3 +</option>
                                                                <option value="4">4 +</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-4 col-md-6 col-sm-12 column">
                                                        <label>Garagens</label>
                                                        <div class="select-box">
                                                            <select class="wide" name="garagem">
                                                                <option value=""
                                                                    data-display="{{ $this->garage === 1 }}">Garagens
                                                                </option>
                                                                <option value="1"
                                                                    {{ $this->garage === 1 ? 'selected' : '' }}>1 +
                                                                </option>
                                                                <option value="2"
                                                                    {{ $this->garage === 2 ? 'selected' : '' }}>2 +
                                                                </option>
                                                                <option value="3"
                                                                    {{ $this->garage === 3 ? 'selected' : '' }}>3 +
                                                                </option>
                                                                <option value="4"
                                                                    {{ $this->garage === 4 ? 'selected' : '' }}>4 +
                                                                </option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
