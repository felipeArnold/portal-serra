@extends('layout.index')

@section('title', 'Imóveis para Alugar em ' . mb_convert_case( $properties->cityFilter , MB_CASE_TITLE , 'UTF-8' )  . ', RS')
@section('description', 'description')

@section('content')

    <section class="page-title-two bg-color-1 centred">
        <div class="pattern-layer">
            <div class="pattern-1" style="background-image: url(assets/images/shape/shape-9.png);"></div>
            <div class="pattern-2" style="background-image: url(assets/images/shape/shape-10.png);"></div>
        </div>
        <div class="auto-container">
            <div class="content-box clearfix">
                <h1 class="text-white h2">Imóveis para Alugar na cidade de {{ mb_convert_case( $properties->cityFilter , MB_CASE_TITLE , 'UTF-8' ) }}, RS</h1>
                <h4 class="text-white">{{ $properties->total() }} imóveis encontrados </h4>
            </div>
        </div>
    </section>
    
    {{-- FILTER PROPERTIE LIVEWARE--}}
    <livewire:filter-component 
    :city="$properties->cityFilter"
    :district="$properties->district"
    :type="$properties->type"
    :order="''"
    :restroom="''"
    :bedroom="''"
    :garage="''"
    />

    <section class="property-page-section property-grid">
        <div class="large-container">
            <div class="row clearfix">

                <div class="col-lg-12 col-md-12 col-sm-12 content-side">

                    <div class="property-content-side">

                        <div class="wrapper grid">

                            <div class="deals-grid-content grid-item property-details property-details-one pt-0 mb-0">

                                <div class="row clearfix">
                                    @foreach ($properties as $propertie)
                                        @php
                                            $urlPropertie = $propertie['city'] . '/' . $propertie['improvement']['properties_key'] . '/' . str_replace(' ', '-', strtolower($propertie['advert']['title']));
                                        @endphp
                                        <div class="col-lg-3 col-md-3 col-sm-12 feature-block">
                                            <div class="feature-block-one">
                                                <div class="inner-box content-side">
                                                    <div class="image-box property-details-content">

                                                        <div class="carousel-inner">

                                                            <div
                                                                class="single-item-carousel owl-carousel owl-theme owl-dots-none">
                                                                @if (count($propertie['image']) > 0)
                                                                    @foreach ($propertie['image'] as $image)
                                                                        @if ($image['order'] <= 5)
                                                                            <figure class="image-box">
                                                                                <img src="https://media.samisistemas.com.br/{{ strtolower($propertie['initials']) }}/data/imagens/aluguel/{{ $image['file'] }}?{{ date('YmdHsi') }}"
                                                                                    alt="{{ $propertie['advert']['title'] }}"
                                                                                    style="max-height: 250px; min-height: 250px; max-width: 100%;">
                                                                            </figure>
                                                                        @endif
                                                                    @endforeach
                                                                @else
                                                                    <img src="https://www.barcellos.com.br/images/nao-disponivel.jpg"
                                                                        alt="{{ $propertie['advert']['title'] }}"
                                                                        style="max-height: 250px; min-height: 250px; max-width: 100%;">
                                                                @endif
                                                            </div>

                                                        </div>
                                                        @if ($propertie['advert']['offer'] == 'S')
                                                            <div class="batch"><i class="icon-11"></i></div>
                                                        @endif
                                                        @if ($propertie['advert']['emphasis'] == 'S')
                                                            <span class="category">DESTAQUE</span>
                                                        @endif
                                                        @if ($propertie['advert']['exclusive'] == 'S')
                                                            <span class="category">EXCLUSIVO</span>
                                                        @endif
                                                        @isset($propertie->hit->views)
                                                            <span class="views">
                                                                <i class="fa fa-eye"></i>
                                                                {{ $propertie->hit->views }}
                                                            </span>
                                                        @endisset
                                                    </div>
                                                    <div class="lower-content">
                                                        <div class="author-info clearfix">
                                                            <div class="author pull-left pl-0">
                                                                <h6>{{ Str::upper($propertie['type']) }}</h6>
                                                            </div>
                                                            <div class="buy-btn pull-right"><a href="{{ $urlPropertie }}">
                                                                    {{ $propertie['improvement']['properties_key'] }}
                                                                </a>
                                                            </div>
                                                        </div>
                                                        <div class="title-text">
                                                            <h4 class="designation">
                                                                <a href="{{ $urlPropertie }}" class="d-flex flex-column">
                                                                    <span>
                                                                        {{ $propertie['address'] . ', ' . $propertie['district'] . ', ' . $propertie['city'] . '/' . $propertie['state'] }}
                                                                    </span>
                                                                </a>
                                                            </h4>
                                                        </div>

                                                        <h3 class="title-description">
                                                            {{ $propertie['advert']['title'] }}
                                                        </h3>

                                                        <ul class="more-details clearfix">
                                                            <li>
                                                                <div class="d-flex flex-column">
                                                                    <span>
                                                                        <i class="icon-14"></i>
                                                                        {{ $propertie['improvement']['dormitory'] }}
                                                                    </span>
                                                                    <span>
                                                                        {{ $propertie['improvement']['dormitory'] == 1 ? ' dormitório' : ' dormitórios' }}
                                                                    </span>
                                                                </div>
                                                            </li>
                                                            <li>
                                                                <div class="d-flex flex-column">
                                                                    <span>
                                                                        <i class="icon-15"></i>
                                                                        {{ $propertie['improvement']['restroom'] }}
                                                                    </span>
                                                                    <span>
                                                                        {{ $propertie['improvement']['restroom'] == 1 ? ' banheiro' : ' banheiro' }}
                                                                    </span>
                                                                </div>
                                                            </li>
                                                            @isset($propertie['area']['private'])
                                                                <li>
                                                                    <div class="d-flex flex-column">
                                                                        <span>
                                                                            <i class="icon-16"></i>
                                                                            {{ $propertie['area']['private'] }} m²
                                                                        </span>
                                                                        <span>
                                                                            Área privada
                                                                        </span>
                                                                    </div>
                                                                </li>
                                                            @endisset
                                                        </ul>
                                                        <div class="price-box clearfix">
                                                            <div class="price-info pull-left d-flex flex-column">
                                                                @if (!is_null($propertie['price']))
                                                                    <h6>Aluguel: R$
                                                                        {{ $propertie['price']['rent'] ? number_format($propertie['price']['rent'], '2', ',', '.') : 'Consultar' }}
                                                                    </h6>
                                                                    <h6>Total: R$
                                                                        {{ number_format($propertie['price']['rent'] + $propertie['price']['condominium'] + $propertie['price']['iptu'], '2', ',', '.') }}
                                                                    </h6>
                                                                @else
                                                                    <h6> Consultar valores </h6>
                                                                @endif
                                                            </div>
                                                            <div class="btn-box pull-right">
                                                                <a href="{{ $urlPropertie }}" class="theme-btn btn-two">
                                                                    Detalhes
                                                                </a>
                                                            </div>
                                                        </div>

                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    @endforeach
                                </div>
                            </div>
                        </div>

                        @if ($properties->total() > 12)
                            <div class="pagination-wrapper mt-0">
                                <nav aria-label="Page navigation example">
                                    <ul class="pagination">
                                        @if ($properties->onFirstPage() === false)
                                            <li class="page-item page-item-preview">
                                                <a class="page-link" href="{{ $properties->previousPageUrl() . $properties->requestFilter }}">
                                                    <i class="fas fa-angle-left"></i>
                                                </a>
                                            </li>
                                        @endif

                                        @for ($i = 1; $i <= $properties->lastPage(); $i++)
                                            @if ($i <= 2 || $i >= $properties->lastPage() - 2 || abs($i - $properties->currentPage()) <= 2)
                                                @php $ellipsis = false @endphp
                                                <li class="page-item">
                                                    <a class="page-link @if ($properties->currentPage() == $i) current @endif"
                                                        href="{{ $properties->url($i). $properties->requestFilter }}">
                                                        {{ $i }}
                                                    </a>
                                                </li>
                                            @else
                                                @if (!$ellipsis)
                                                    <li class="page-item">
                                                        <a class="page-link">
                                                            ...
                                                        </a>
                                                    </li>
                                                @endif
                                                @php $ellipsis = true @endphp
                                            @endif
                                        @endfor

                                        @if ($properties->hasMorePages())
                                            <li class="page-item">
                                                <a class="page-link"
                                                    href="{{ $properties->nextPageUrl() . $properties->requestFilter }}">
                                                    <i class="fas fa-angle-right"></i>
                                                </a>
                                            </li>
                                        @endif
                                    </ul>
                                </nav>
                            </div>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </section>

@endsection
