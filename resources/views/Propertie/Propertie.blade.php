@extends('layout.index')

@section('title', '' . $propertie->advert->title . '')
@section('description', 'description')

@section('content')

    <link href="{{ asset('assets/bibliotecas/lightgallery/css/lightgallery.css') }}" rel="stylesheet">

    <!--Page Title-->
    <section class="page-title-two bg-color-1 centred">
        <div class="pattern-layer">
            <div class="pattern-1" style="background-image: url(assets/images/shape/shape-9.png);"></div>
            <div class="pattern-2" style="background-image: url(assets/images/shape/shape-10.png);"></div>
        </div>

        <div class="auto-container">
            <div class="content-box clearfix">
                <h1 class="text-white">{{ $propertie->advert->title }}</h1>
                <ul class="bread-crumb clearfix">
                    <li class="text-white"><a class="text-white" href="/index">Home</a></li>
                    <li class="text-white"><a class="text-white" href="/imoveis-alugar">Alugar</a></li>
                    <li class="text-white"><a class="text-white" href="/imoveis-alugar">{{ $propertie->district }}</a>
                    </li>
                    <li class="text-white"><a class="text-white"
                            href="/imoveis-alugar">{{ $propertie->city . ', ' . $propertie->state }}</a></li>
                </ul>
            </div>
        </div>
    </section>
    <!--End Page Title-->

    <section class="slider" style="overflow: hidden;">
        <div class="flexslider carousel ">
            <ul id="lightgallery" class="slides">
                @if ($propertie->image->count() > 0)
                    @foreach ($propertie->image as $image)
                        @php
                            $filePropertie = 'https://media.samisistemas.com.br/' . strtolower($propertie->initials) . '/data/imagens/aluguel/' . $image->file . '?' . date('YmdHsi');
                        @endphp
                        <li class="resp" data-responsive="{{ $filePropertie }}" data-src="{{ $filePropertie }}"
                            data-sub-html="<p>{{ $propertie->advert->title }}</p>">
                            <a href="" class="example-image-link">
                                <img class="img-responsive" src="{{ $filePropertie }}"
                                    style="max-height: 250px; min-height: 250px" alt="{{ $propertie->advert->title }}">
                            </a>
                        </li>
                    @endforeach
                @endif
            </ul>
        </div>
    </section>


    <!-- PROPERTY-DETAILS -->
    <section class="property-details property-details-two">
        <div class="auto-container">

            <div class="top-details clearfix">
                <div class="left-column pull-left clearfix">

                    <div class="author-info clearfix">
                        <div class="pull-left">
                            <h5>{{ $propertie->advert->title }}</h5>
                            <h6>
                                <span>{{ $propertie->address . ', ' . $propertie->district . ', ' }}</span>
                                <span>{{ $propertie->city . '/' . $propertie->state }}</span>
                            </h6>

                            @if ($propertie->hit->count() > 0)
                                <div class="price-box clearfix mt-3">
                                    <div class="price-info pull-left d-flex flex-column">
                                        <h6>
                                            <b>
                                                {{ $propertie->hit->views . ($propertie->hit->views == 1 ? ' visualização' : ' visualizações') }}
                                            </b>
                                        </h6>
                                    </div>
                                </div>
                            @endif
                        </div>
                    </div>
                </div>

                <div class="right-column pull-right clearfix">
                    <div class="price-inner clearfix">
                        <ul class="category clearfix pull-left">
                            @if ($propertie->advert->emphasis == 'S')
                                <li><a>DESTAQUE</a></li>
                            @endif
                            @if ($propertie->advert->exclusive == 'S')
                                <li><a>EXCLUSIVO</a></li>
                            @endif
                        </ul>

                        <div class="price-box pull-right">
                            <div class="author-info clearfix">
                                <div class="author-box pull-left d-flex flex-column">
                                    <h6>Aluguel: R$ {{ number_format($propertie->price->rent, '2', ',', '.') }} </h6>
                                    <h6>Total:
                                        {{ number_format($propertie->price->rent + $propertie->price->condominium + $propertie->price->iptu, '2', ',', '.') }}
                                    </h6>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>

            <div class="col-md-12 mb-5">
                <div class="flex-space" style="justify-content: flex-start;">
                    @if ($propertie->improvement->properties_key)
                        <div class="benfeitorias flex-column align-center elementoTela43">
                            <span>Código</span>
                            <b>{{ $propertie->improvement->properties_key }}</b>
                        </div>
                    @endif
                    @if ($propertie->improvement->dormitory)
                        <div class="benfeitorias flex-column align-center elementoTela43">
                            <i class="fas fa-bed-alt"></i>
                            {{ $propertie->improvement->dormitory }} Quartos
                        </div>
                    @endif
                    @if ($propertie->improvement->restroom)
                        <div class="benfeitorias flex-column align-center elementoTela43">
                            <i class="fa-light fa-bath"></i>
                            {{ $propertie->improvement->restroom }} Banheiros
                        </div>
                    @endif
                    @if ($propertie->improvement->suite)
                        <div class="benfeitorias flex-column align-center elementoTela43">
                            <i class="fas fa-bath"></i>
                            {{ $propertie->improvement->suite }} Suítes
                        </div>
                    @endif
                    @if ($propertie->improvement->living_room)
                        <div class="benfeitorias flex-column align-center elementoTela43">
                            <i class="fal fa-home-lg-alt"></i>
                            {{ $propertie->improvement->living_room }} Salas
                        </div>
                    @endif
                    @if ($propertie->improvement->box)
                        <div class="benfeitorias flex-column align-center elementoTela43">
                            <i class="fas fa-car"></i>
                            {{ $propertie->improvement->box }} Garagem
                        </div>
                    @endif
                    @if ($propertie->area->private)
                        <div class="benfeitorias flex-column align-center elementoTela43">
                            <i class="fal fa-ruler-horizontal"></i>
                            {{ $propertie->area->private }} m²
                        </div>
                    @endif
                </div>
            </div>

            <div class="row clearfix">

                <div class="col-lg-8 col-md-12 col-sm-12 content-side">

                    <div class="property-details-content">

                        <div class="details-box content-widget">
                            <div class="title-box">
                                <h4>Valores</h4>
                            </div>

                            <ul class="list clearfix">
                                @if ($propertie->price->rent)
                                    <li>Aluguel: <span> {{ number_format($propertie->price->rent, '2', ',', '.') }}
                                        </span></li>
                                @endif
                                @if ($propertie->price->condominium)
                                    <li>Condomínio: <span>
                                            {{ number_format($propertie->price->condominium, '2', ',', '.') }}
                                        </span></li>
                                @endif
                                @if ($propertie->price->iptu)
                                    <li>IPTU: <span> {{ number_format($propertie->price->iptu, '2', ',', '.') }}
                                        </span></li>
                                @endif
                                @if ($propertie->improvement->dormitory)
                                    <li>Total:
                                        <span>{{ number_format($propertie->price->rent + $propertie->price->condominium + $propertie->price->iptu, '2', ',', '.') }}
                                        </span>
                                    </li>
                                @endif
                            </ul>

                        </div>

                        <div class="details-box content-widget">
                            <div class="title-box">
                                <h4>Detalhes do imóvel</h4>
                            </div>

                            <ul class="list clearfix">
                                @if ($propertie->improvement->properties_key)
                                    <li>Imóvel: <span> {{ $propertie->improvement->properties_key }} </span></li>
                                @endif
                                @if ($propertie->type)
                                    <li>Tipo: <span> {{ $propertie->type }} </span></li>
                                @endif
                                @if ($propertie->improvement->dormitory)
                                    <li>Quartos: <span>{{ $propertie->improvement->dormitory }} </span></li>
                                @endif
                                @if ($propertie->improvement->restroom)
                                    <li>Banheiros: <span> {{ $propertie->improvement->restroom }}</span></li>
                                @endif
                                @if ($propertie->improvement->suite)
                                    <li>Suíte: <span>{{ $propertie->improvement->suite }}</span></li>
                                @endif
                                @if ($propertie->improvement->living_room)
                                    <li>Salas: <span>{{ $propertie->improvement->living_room }}</span></li>
                                @endif
                                @if ($propertie->improvement->box)
                                    <li>Garagem: <span>{{ $propertie->improvement->box }}</span></li>
                                @endif
                                @if ($propertie->area->common)
                                    <li>Área Comum: <span>{{ $propertie->area->common }} m²</span></li>
                                @endif
                                @if ($propertie->area->private)
                                    <li>Área privativa: <span>{{ $propertie->area->private }} m²</span></li>
                                @endif
                                @if ($propertie->area->total_land)
                                    <li>Área total: <span>{{ $propertie->area->total_land }} m²</span></li>
                                @endif
                                @if ($propertie->area->terrain_dimension)
                                    <li>Dimensão do Terreno: <span>
                                            {{ $propertie->area->terrain_dimension }}
                                            m²</span>
                                    </li>
                                @endif
                            </ul>

                        </div>

                        <div class="discription-box content-widget">
                            <div class="title-box">
                                <h4>Descrição</h4>
                            </div>
                            <div class="text">
                                <p>
                                    {{ $propertie->advert->announcement }}
                                </p>
                            </div>
                        </div>

                        <div class="location-box content-widget">

                            <div class="title-box">
                                <h4>Endereço</h4>
                            </div>

                            <ul class="info clearfix d-flex flex-column">
                                @if ($propertie->address)
                                    <li><span>Logradouro:</span> {{ $propertie->address }} </li>
                                @endif
                                @if ($propertie->district)
                                    <li><span>Bairro:</span>{{ $propertie->district }}</li>
                                @endif
                                @if ($propertie->zip_code)
                                    <li><span>CEP:</span> {{ $propertie->zip_code }}</li>
                                @endif
                                @if ($propertie->city)
                                    <li><span>Cidade:</span> {{ $propertie->city }} </li>
                                @endif
                                @if ($propertie->state)
                                    <li><span>Estado:</span> {{ $propertie->state }}</li>
                                @endif
                            </ul>

                            <section id="mapaEndereco">
                                <address>
                                    {{ utf8_encode($propertie->address . ', ' . $propertie->district . ', ' . $propertie->city . '/' . $propertie->state) }}
                                </address>
                            </section>
                        </div>

                    </div>
                </div>

                <div class="col-lg-4 col-md-12 col-sm-12 sidebar-side">
                    <div class="property-sidebar default-sidebar">
                        <div class="author-widget sidebar-widget">
                            <div class="author-box pl-0">
                                <div class="inner">
                                    <h4>Faça sua proposta</h4>

                                    <div>
                                        <div class="btn-box btn-block">
                                            <a class="btn btn-block" href="agents-details.html">Faça sua proposta</a>
                                        </div>
                                        <div class="btn-box btn-block">
                                            <a class="btn btn-block" href="agents-details.html">Alugue Online</a>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>

        </div>
    </section>

    {{-- SIMILAR REAL ESTATE LIVEWARE  --}}
    <livewire:propertie-component 
        :title="'Similares'"
        :description="$propertie->type . ' similares'"
        :city="$propertie->city"
        :district="$propertie->district"
        :type="$propertie->type"
    />
    

    <!-- PROPERTY-DETAILS END -->
    <script>
        consultaEndereco();

        $(document).ready(function() {
            $('#lightgallery').lightGallery();
        });
    </script>
    <script src="{{ asset('assets/bibliotecas/lightgallery/js/lightgallery-all.min.js') }}"></script>
    <script>
        $(window).load(function() {
            $('.flexslider').flexslider({
                animation: "slide",
                animationLoop: false,
                itemWidth: 210,
                maxHeight: 210,
                itemMargin: 5,
                minItems: 2,
                maxItems: 5,
                start: function(slider) {
                    $('body').removeClass('loading');
                }
            });
        });
    </script>
@endsection
