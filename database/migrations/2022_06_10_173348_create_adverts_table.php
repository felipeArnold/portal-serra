<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAdvertsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('adverts', function (Blueprint $table) {
            
            $table->id();
            $table->string('properties_key', 10)->index();
            $table->text('title');
            $table->longText('announcement');
            $table->longText('offer_phrase');
            $table->enum('emphasis', ['S', 'N']);
            $table->enum('offer', ['S', 'N']);
            $table->enum('exclusive', ['S', 'N']);
            $table->enum('accompanied_visit', ['S', 'N']);
            $table->enum('new_used', ['S', 'N']);
            $table->unique('properties_key');
            $table->foreign('properties_key')->references('key')->on('properties');
        });
    }

    public function down()
    {
        Schema::dropIfExists('adverts');
    }
}
