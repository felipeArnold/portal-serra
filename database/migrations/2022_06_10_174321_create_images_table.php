<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateImagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('images', function (Blueprint $table) {
            
            $table->id();
            $table->string('properties_key', 10)->index();
            $table->enum('goal', ['LOC', 'VEND', 'TEMP']);
            $table->string('file');
            $table->integer('order');
            $table->foreign('properties_key')->references('key')->on('properties');
        });
    }

    public function down()
    {
        Schema::dropIfExists('images');
    }
}
