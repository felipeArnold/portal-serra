<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateImprovementsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('improvements', function (Blueprint $table) {
            
            $table->id('id');
            $table->string('properties_key', 10)->index();
            $table->integer('dormitory');
            $table->integer('restroom');
            $table->integer('suite');
            $table->integer('living_room');
            $table->integer('box');
            $table->string('solar_position', 3);
            $table->string('construction_type', 3);
            $table->string('floor_type', 3);
            $table->string('property_position', 3);
            $table->foreign('properties_key')->references('key')->on('properties');
        });
    }

    public function down()
    {
        Schema::dropIfExists('improvements');
    }
}
