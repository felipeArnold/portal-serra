<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePropertiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('properties', function (Blueprint $table) {
            
            $table->id();
            $table->char('initials', 3);
            $table->string('key', 10)->index();
            $table->string('type', 100);
            $table->enum('goal', ['LOC', 'VEND', 'TEMP']);
            $table->string('address', 50);
            $table->string('number', 10);
            $table->string('district', 50);
            $table->string('city', 50);
            $table->char('state', 2);
            $table->string('zip_code', 8);
            $table->string('condominium', 50);
            $table->string('property_url');
            $table->string('city_url');
            $table->string('district_url');
            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('properties');
    }
}
