<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAreasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('areas', function (Blueprint $table) {
            
            $table->id();
            $table->string('properties_key', 10)->index();
            $table->decimal('common', $precision = 10, $scale = 2);
            $table->decimal('private', $precision = 10, $scale = 2);
            $table->decimal('terrain_dimension', $precision = 10, $scale = 2);
            $table->decimal('total_land', $precision = 10, $scale = 2);
            $table->unique('properties_key');
            $table->foreign('properties_key')->references('key')->on('properties');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('areas');
    }
}
