<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCompaniesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('companies', function (Blueprint $table) {
            $table->id();
            $table->char('initials', 3);
            $table->string('name', 100);
            $table->string('social_reason', 100);
            $table->string('address', 50);
            $table->string('number', 10);
            $table->string('district', 50);
            $table->string('city', 50);
            $table->char('state', 2);
            $table->string('zip_code', 8);
            $table->string('phone', 11);
            $table->string('growth', 11);
            $table->string('site', 50);
            $table->string('url');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('companies');
    }
}
