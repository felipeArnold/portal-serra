<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePricesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('prices', function (Blueprint $table) {
            
            $table->id();
            $table->string('properties_key', 10)->index();
            $table->decimal('rent', $precision = 10, $scale = 2);
            $table->decimal('iptu', $precision = 10, $scale = 2);
            $table->decimal('condominium', $precision = 10, $scale = 2);
            $table->decimal('others', $precision = 10, $scale = 2);
            $table->enum('value_on_request', ['Y', 'N']);
            $table->unique('properties_key');
            $table->foreign('properties_key')->references('key')->on('properties');
        });
    }

    public function down()
    {
        Schema::dropIfExists('prices');
    }
}
